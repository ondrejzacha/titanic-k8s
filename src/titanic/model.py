# -*- coding: utf-8 -*-

"""Classes for creating and persisting (fitted) ML models."""

import joblib
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import SimpleImputer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.utils.validation import check_is_fitted


class TitanicModel:
    """A RandomForest-based model for predicting survival in the Titanic dataset."""

    def __init__(self, version=None):
        self.pipeline = self.create_pipeline()
        self.version = version

    def fit(self, X, y):
        """
        Fits model on given dataset.

        Parameters
        ----------
        X : pd.Dataframe
            Dataframe containing training data (features only, no response).
        y : Union[pd.Series, np.ndarray]
            Pandas series (or numpy array) containing the response values for the
            given training dataset.

        Returns
        -------
        Model
            Returns the model itself, after fitting.
        """
        self.pipeline.fit(X, y)
        return self

    def predict(self, X):
        """
        Produces predictions for the given dataset.

        Parameters
        ----------
        X : pd.DataFrame
            Dataframe to produce predictions for.
        """
        check_is_fitted(self.pipeline, attributes=["classes_"])
        return self.pipeline.predict(X)

    @staticmethod
    def create_pipeline():
        """
        Creates ML pipeline.
        """
        numeric_pipeline = make_pipeline(SimpleImputer(), StandardScaler())

        text_pipeline = make_pipeline(
            SimpleImputer(strategy="most_frequent"),
            OneHotEncoder(handle_unknown="ignore", sparse=False),
        )

        pipeline = make_pipeline(
            ColumnTransformer(
                [
                    (
                        "numeric_transformer",
                        numeric_pipeline,
                        ["Age", "SibSp", "Parch", "Fare"],
                    ),
                    ("text_transformer", text_pipeline, ["Pclass", "Embarked", "Sex"]),
                ]
            ),
            RandomForestClassifier(max_depth=10, n_estimators=200),
        )
        return pipeline

    @classmethod
    def load(cls, file_path):
        """Loads model fit from given file path.

        Parameters
        ----------
        file_path : str
            Path to a pickled model file.

        Returns
        -------
        ModelFit
            The unpickled model instance.

        """
        return joblib.load(file_path)

    def save(self, file_path):
        """Saves model fit to given file path.

        Parameters
        ----------
        file_path : str
            Path to save the pickled model to.

        """
        joblib.dump(self, file_path)
