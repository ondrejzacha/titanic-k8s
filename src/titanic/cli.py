# -*- coding: utf-8 -*-

"""Console script for titanic."""

import glob
import logging

import click
import pandas as pd

from .model import TitanicModel

logger = logging.getLogger(__name__)

VERSION = "0"


@click.command()
@click.option("--version", "-V", default=None)
def train(version):
    """Command line interface for training of the titanic model."""

    version = version or VERSION
    logging.basicConfig(level=logging.INFO)

    datasets = [pd.read_csv(f) for f in glob.glob("/data/*.csv")]
    dataset = pd.concat(datasets)

    x_train = dataset.drop(["Survived"], axis=1)
    y_train = dataset["Survived"]
    logger.info(x_train.columns)

    model_fit = TitanicModel(version=version).fit(x_train, y_train)

    output_path = f"/models/model__{version}.pkl"
    model_fit.save(output_path)


if __name__ == "__main__":
    train()
