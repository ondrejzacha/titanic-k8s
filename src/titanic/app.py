"""Functions for creating the Flask application."""

import os
from typing import List

import pandas as pd
from fastapi import Depends, FastAPI
from pydantic import BaseModel
from titanic.model import TitanicModel

app = FastAPI()


class DataRow(BaseModel):
    PassengerId: int
    Pclass: int
    Name: str
    Sex: str
    Age: float = None
    SibSp: int
    Parch: int
    Ticket: str
    Fare: float = None
    Cabin: str = None
    Embarked: str


async def load_titanic_model():
    model_version = os.environ.get("MODEL_VERSION", "1")
    model_path = f"/models/model__{model_version}.pkl"
    model = TitanicModel().load(model_path)
    return model


@app.get("/ping")
async def ping(titanic_model: TitanicModel = Depends(load_titanic_model)):
    """Heartbeat endpoint."""
    return f"Hello from version {titanic_model.version}", 200


@app.post("/predict")
async def predict(
    datarows: List[DataRow], titanic_model: TitanicModel = Depends(load_titanic_model)
):
    """Predict endpoint, which produces predictions for a given dataset."""
    data = pd.DataFrame.from_dict([r.dict() for r in datarows])
    y_pred = titanic_model.predict(data)
    return {
        "prediction": [float(yp) for yp in y_pred],
        "metadata": {"model_version": titanic_model.version},
    }
