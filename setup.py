from setuptools import find_packages, setup

setup(
    name="titanic",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    install_requires=open("requirements.txt").readlines(),
    extras_require={"dev": open("requirements-dev.txt").readlines()},
    description="",
    entry_points={"console_scripts": ["train = titanic.cli:train"]},
    long_description_content_type="text/markdown",
)
