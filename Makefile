clean:
	rm -rf .ipynb_checkpoints
	rm -rf */.ipynb_checkpoints
	rm -rf src/titanic.egg-info
	rm -rf .pytest_cache

install:
	pip install .

develop:
	pip install -e ".[dev]"
	pre-commit install

format:
	black src/titanic setup.py
	isort src/titanic/*.py setup.py

check:
	black --check src/titanic setup.py
	flake8 src/titanic setup.py

test:
	pytest
