# Reference Kubernetes model deployment project

- Assumes `docker-desktop` kubernetes cluster

### Basic setup
- Set up Docker registry:
    - Run locally on `localhost:5000` (k8s setup assumes this option):
        ```bash
        docker run -d -p 5000:5000 --restart=always --name registry registry:2
        ```
    - or use Docker Hub, Amazon ECR, GCP Container Registry etc.
- Build and push image:
    ```bash
    docker build -t <registry>/titanic:<version>
    docker push <registry>/titanic:<version>
    ```
- Set up persistent volumes: `kubectl -f apply k8s/volumes.yaml`
- Place training data in `/tmp/pv_data`
- Run training job `kubectl -f apply k8s/training-job.yaml`
    - creates a `model__<version>.pkl` file in `/tmp/pv_models`
- Deploy default model service: `kubectl -f apply k8s/deployment-default.yaml`
- Test:
    - `kubectl get service/titanic-svc` to see assigned port --> `http://localhost:<port>/ping`
    - or: `curl "http://localhost:$(kubectl get service/titanic-svc -o=jsonpath={'.spec.ports[].nodePort'})/ping"`

### Deployment of a second model
- Change code
  - Update code in repo
  - `docker build -t <registry>/titanic:<version>`
  - `docker push <registry>/titanic:<version>`
- and/or change training data
  - Update data in `/tmp/pv_data` (or write a job to do that)
- Create and run training job with version=`<version>`
- Set MODEL_VERSION environment variable (& image if necessary) in `k8s/deployment-canary.yaml`
- Deploy: `kubectl -f apply k8s/deployment-canary.yaml`
- Test:
    - `kubectl get service/titanic-svc-canary` to see assigned port --> `http://localhost:<port>/ping`
    - or: `curl "http://localhost:$(kubectl get service/titanic-svc-canary -o=jsonpath={'.spec.ports[].nodePort'})/ping"`

### Ingress setup
- Add hosts to local hosts file:
  - add following lines to `/etc/hosts`:
    ```
    127.0.0.1 kubernetes.docker.internal
    127.0.0.1 canary.kubernetes.docker.internal  # only needed for multihost scenario
    ```
- Two scenarios:
  - Multiple hosts: `kubectl apply -f k8s/ingress-multihost.yaml`
  - Single host, traffic splitting: `kubectl apply -f k8s/ingress-traffic-split.yaml`

---
Adapted from [GoDataDriven productionizing exercise](https://github.com/godatadriven/code-breakfast-productionizing)
