import numpy as np
import pandas as pd
import pytest
from titanic.model import TitanicModel


@pytest.fixture
def fake_titanic():
    return pd.DataFrame(
        {
            "PassengerId": {5: 6, 6: 7, 7: 8, 8: 9, 9: 10},
            "Survived": {5: 0, 6: 0, 7: 0, 8: 1, 9: 1},
            "Pclass": {5: 3, 6: 1, 7: 3, 8: 3, 9: 2},
            "Name": {
                5: "Moran, Mr. James",
                6: "McCarthy, Mr. Timothy J",
                7: "Palsson, Master. Gosta Leonard",
                8: "Johnson, Mrs. Oscar W (Elisabeth Vilhelmina Berg)",
                9: "Nasser, Mrs. Nicholas (Adele Achem)",
            },
            "Sex": {5: "male", 6: "male", 7: "male", 8: "female", 9: "female"},
            "Age": {5: np.nan, 6: 54.0, 7: 2.0, 8: 27.0, 9: 14.0},
            "SibSp": {5: 0, 6: 0, 7: 3, 8: 0, 9: 1},
            "Parch": {5: 0, 6: 0, 7: 1, 8: 2, 9: 0},
            "Ticket": {5: "330877", 6: "17463", 7: "349909", 8: "347742", 9: "237736"},
            "Fare": {5: 8.4583, 6: 51.8625, 7: 21.075, 8: 11.1333, 9: 30.0708},
            "Cabin": {5: np.nan, 6: "E46", 7: np.nan, 8: np.nan, 9: np.nan},
            "Embarked": {5: "Q", 6: "S", 7: "S", 8: "S", 9: "C"},
        }
    )


def test_fit_works(fake_titanic):
    tm = TitanicModel()
    X, y = fake_titanic.drop(columns="Survived"), fake_titanic["Survived"]
    tm.fit(X, y)

    assert hasattr(tm.pipeline, "classes_")


def test_predictions_are_an_array(fake_titanic):
    tm = TitanicModel()
    X, y = fake_titanic.drop(columns="Survived"), fake_titanic["Survived"]
    tm.fit(X, y)
    predictions = tm.predict(X)

    assert isinstance(predictions, np.ndarray)
